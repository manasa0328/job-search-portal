package com.job.search.portal.services;
import com.job.search.portal.domain.model.JobPostedDetailsBo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KafkaProducer {
    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaProducer.class);

    @Autowired
    private KafkaTemplate<String, List<JobPostedDetailsBo>> kafkaTemplate;

    @Value("${kafka.topic.name}")
    String kafkaTopic = "bulk-job-details";

    public void send(List<JobPostedDetailsBo> data) {
        LOGGER.info("sending data='{}'", data);
        kafkaTemplate.send(kafkaTopic, data);
    }
}