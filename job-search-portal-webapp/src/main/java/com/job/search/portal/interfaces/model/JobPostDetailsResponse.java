package com.job.search.portal.interfaces.model;

public class JobPostDetailsResponse {
    String responseCode;
    String responseDescription;
    String errorResponse;

    public JobPostDetailsResponse(String responseCode, String responseDescription) {
        this.responseCode = responseCode;
        this.responseDescription = responseDescription;
    }

    public JobPostDetailsResponse(String responseCode, String responseDescription, String errorResponse) {
        this.responseCode = responseCode;
        this.responseDescription = responseDescription;
        this.errorResponse = errorResponse;
    }

    public String getErrorResponse() {
        return errorResponse;
    }

    public void setErrorResponse(String errorResponse) {
        this.errorResponse = errorResponse;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseDescription() {
        return responseDescription;
    }

    public void setResponseDescription(String responseDescription) {
        this.responseDescription = responseDescription;
    }
}
