package com.job.search.portal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan({ "com.job" })
@EnableJpaRepositories
@EntityScan("com.job.search.portal.domain.entity")

public class JobSearchPortalApplication {

    public static void main(String[] args) {
		SpringApplication.run(JobSearchPortalApplication.class, args);
	}
}