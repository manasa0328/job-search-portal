package com.job.search.portal.interfaces.controller;

import com.job.search.portal.application.services.JobSearchService;
import com.job.search.portal.domain.entity.JobPostedDetails;
import com.job.search.portal.domain.model.JobPostedDetailsBo;
import com.job.search.portal.infrastructure.externals.JobPortalResponseCode;
import com.job.search.portal.interfaces.model.JobPostDetailsRequest;
import com.job.search.portal.interfaces.model.JobPostDetailsResponse;
import com.job.search.portal.services.KafkaProducer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/job-search")
public class JobSearchPortalController {
    private static final Log LOGGER = LogFactory.getLog(JobSearchPortalController.class);

    @Autowired JobSearchService jobSearchService;
    @Autowired KafkaProducer producer;

    @PostMapping(value = "/job-post-details")
    public ResponseEntity<?> jobPostDetails(@Valid @RequestBody JobPostDetailsRequest jobPostDetailsRequest,
                                            BindingResult bindingResult) {
        JobPostDetailsResponse jobPostDetailsResponse;
        try {
            if(bindingResult.hasErrors()) {
                LOGGER.error("Error occurred during Json validation ");
                FieldError fieldError = bindingResult.getFieldError();
                jobPostDetailsResponse = new JobPostDetailsResponse(JobPortalResponseCode.INVALID_JSON.getResponseCode(),
                        JobPortalResponseCode.INVALID_JSON.getResponseDescription(), fieldError.getDefaultMessage());
                return new ResponseEntity<>(jobPostDetailsResponse, HttpStatus.CONFLICT);
            } else {
                JobPostedDetailsBo jobPostedDetailsBo = constructJobDetails(jobPostDetailsRequest);
                boolean isProcessed = jobSearchService.processJobPostDetails(jobPostedDetailsBo);
                if (isProcessed) {
                    return new ResponseEntity<>(new JobPostDetailsResponse(JobPortalResponseCode.SUCCESS.getResponseCode(),
                            JobPortalResponseCode.SUCCESS.getResponseDescription()), HttpStatus.ACCEPTED);
                } else {
                    return new ResponseEntity<>(new JobPostDetailsResponse(JobPortalResponseCode.FAIL.getResponseCode(),
                            JobPortalResponseCode.FAIL.getResponseDescription()),HttpStatus.BAD_REQUEST);
                }
            }
        } catch (Exception exception) {
            LOGGER.error("Exception Occurred during job posting in the controller.." + exception);
            return new ResponseEntity<>(new JobPostDetailsResponse(JobPortalResponseCode.EXCEPTION.getResponseCode(),
                    JobPortalResponseCode.EXCEPTION.getResponseDescription(), exception.getLocalizedMessage()),
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/job-post-details/searchBy")
    public ResponseEntity<?> jobPostDetails(@RequestParam(value = "skillSet", required = false, defaultValue = "") String skillSet,
                                            @RequestParam(value = "companyName", required = false, defaultValue = "") String companyName,
                                            @RequestParam(value = "isActive", required = false, defaultValue = "") String isActive,
                                            @RequestParam(value = "jobLocation", required = false, defaultValue = "") String jobLocation,
                                            @RequestParam(value = "jobType", required = false, defaultValue = "") String jobType) {
        try {
            List<JobPostedDetails> jobPostedDetails = jobSearchService.searchJobPostedDetailsByKeyword(skillSet.
                            toUpperCase(), companyName.toUpperCase(), isActive.toUpperCase(), jobLocation.toUpperCase(),
                    jobType.toUpperCase());
            if (!jobPostedDetails.isEmpty()) {
                return new ResponseEntity<>(jobPostedDetails, HttpStatus.ACCEPTED);
            } else {
                return new ResponseEntity<>("Error or No data found while retrieving JOB POSTED DETAILS..",
                        HttpStatus.BAD_REQUEST);
            }
        } catch (Exception exception) {
            LOGGER.error("Exception Occurred while getting job post details.." + exception);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/bulk-upload-job-details")
    public ResponseEntity<?> bulkUploadJobPostDetails(@RequestPart(value = "file") MultipartFile multipartFile)
            throws IOException {
        List<JobPostedDetailsBo> jobPostedDetailsBos = jobSearchService.publishBulkJobDetails(multipartFile);
        producer.send(jobPostedDetailsBos);
        return new ResponseEntity<>("Data is published successfully to Kafka Producer", HttpStatus.ACCEPTED);
    }

    private JobPostedDetailsBo constructJobDetails(JobPostDetailsRequest jobPostDetailsRequest) {
        JobPostedDetailsBo jobPostedDetailsBo = new JobPostedDetailsBo();
        jobPostedDetailsBo.setJobLocation(jobPostDetailsRequest.getJobLocation());
        jobPostedDetailsBo.setJobType(jobPostDetailsRequest.getJobType());
        jobPostedDetailsBo.setPostedDate(System.currentTimeMillis());
        jobPostedDetailsBo.setIsActive(jobPostDetailsRequest.getIsActive());
        jobPostedDetailsBo.setCompanyName(jobPostDetailsRequest.getCompanyName());
        jobPostedDetailsBo.setJobDesc(jobPostDetailsRequest.getJobDesc());
        jobPostedDetailsBo.setUserDetailsId(jobPostDetailsRequest.getUserDetailsId());
        jobPostedDetailsBo.setSkillSet(jobPostDetailsRequest.getSkills().getSkillSet());
        jobPostedDetailsBo.setSkillSetLevel(jobPostDetailsRequest.getSkills().getSkillLevel());
        return jobPostedDetailsBo;
    }
}
