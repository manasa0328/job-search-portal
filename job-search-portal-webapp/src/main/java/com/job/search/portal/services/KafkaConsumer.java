package com.job.search.portal.services;

import com.job.search.portal.application.services.JobSearchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class KafkaConsumer {
    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaProducer.class);

    @Autowired
    JobSearchService jobSearchService;

    @KafkaListener(topics = "${kafka.topic.name}")
    public void processMessage(List<Map<Object,Object>> content) throws Exception {
        LOGGER.info("received content = '{}'", content);
        jobSearchService.uploadBulkJobDetailsIntoDB(content);
    }
}