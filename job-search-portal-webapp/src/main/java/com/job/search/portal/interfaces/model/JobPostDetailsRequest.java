
package com.job.search.portal.interfaces.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "jobLocation",
        "postedDate",
        "isActive",
        "jobType",
        "companyName",
        "jobDesc",
        "userDetailsId",
        "skills"
})
public class JobPostDetailsRequest {

    @NotBlank(message = "Job Location field should not be null or empty")
    @JsonProperty("jobLocation")
    private String jobLocation;

    @JsonProperty("postedDate")
    private String postedDate;

    @NotBlank(message = "Job isActive field should not be null or empty")
    @JsonProperty("isActive")
    private String isActive;

    @JsonProperty("jobType")
    private String jobType;

    @JsonProperty("companyName")
    private String companyName;

    @JsonProperty("jobDesc")
    private String jobDesc;

    @NotBlank(message = "UserDetailsId field should not be null or empty")
    @JsonProperty("userDetailsId")
    private String userDetailsId;

    @JsonProperty("skills")
    private Skills skills;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("jobLocation")
    public String getJobLocation() {
        return jobLocation;
    }

    @JsonProperty("jobLocation")
    public void setJobLocation(String jobLocation) {
        this.jobLocation = jobLocation;
    }

    @JsonProperty("postedDate")
    public String getPostedDate() {
        return postedDate;
    }

    @JsonProperty("postedDate")
    public void setPostedDate(String postedDate) {
        this.postedDate = postedDate;
    }

    @JsonProperty("isActive")
    public String getIsActive() {
        return isActive;
    }

    @JsonProperty("isActive")
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    @JsonProperty("jobType")
    public String getJobType() {
        return jobType;
    }

    @JsonProperty("jobType")
    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    @JsonProperty("companyName")
    public String getCompanyName() {
        return companyName;
    }

    @JsonProperty("companyName")
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    @JsonProperty("jobDesc")
    public String getJobDesc() {
        return jobDesc;
    }

    @JsonProperty("jobDesc")
    public void setJobDesc(String jobDesc) {
        this.jobDesc = jobDesc;
    }

    @JsonProperty("userDetailsId")
    public String getUserDetailsId() {
        return userDetailsId;
    }

    @JsonProperty("userDetailsId")
    public void setUserDetailsId(String userDetailsId) {
        this.userDetailsId = userDetailsId;
    }

    @JsonProperty("skills")
    public Skills getSkills() {
        return skills;
    }

    @JsonProperty("skills")
    public void setSkills(Skills skills) {
        this.skills = skills;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
    @Override
    public String toString() {
        return "JobPostDetailsRequest{" +
                "jobLocationId='" + jobLocation + '\'' +
                ", postedDate='" + postedDate + '\'' +
                ", isActive='" + isActive + '\'' +
                ", jobTypeId='" + jobType + '\'' +
                ", companyName='" + companyName + '\'' +
                ", jobDesc='" + jobDesc + '\'' +
                ", userDetailsId='" + userDetailsId + '\'' +
                ", skills=" + skills +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}