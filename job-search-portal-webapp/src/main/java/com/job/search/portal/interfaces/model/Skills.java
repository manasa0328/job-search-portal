
package com.job.search.portal.interfaces.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "skillSet",
        "skillLevel"
})
public class Skills {

    @JsonProperty("skillSet")
    private String skillSet;
    @JsonProperty("skillLevel")
    private String skillLevel;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("skillSet")
    public String getSkillSet() {
        return skillSet;
    }

    @JsonProperty("skillSet")
    public void setSkillSet(String skillSet) {
        this.skillSet = skillSet;
    }

    @JsonProperty("skillLevel")
    public String getSkillLevel() {
        return skillLevel;
    }

    @JsonProperty("skillLevel")
    public void setSkillLevel(String skillLevel) {
        this.skillLevel = skillLevel;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}