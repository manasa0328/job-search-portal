package com.job.search.portal.domain.model;

public class JobPostedDetailsBo {
    private String jobLocation;
    private String isActive;
    private String jobType;
    private String companyName;
    private long postedDate;
    private String jobDesc;
    private String userDetailsId;
    private String skillSet;
    private String skillSetLevel;

    public String getSkillSet() {
        return skillSet;
    }

    public void setSkillSet(String skillSet) {
        this.skillSet = skillSet;
    }

    public String getSkillSetLevel() {
        return skillSetLevel;
    }

    public void setSkillSetLevel(String skillSetLevel) {
        this.skillSetLevel = skillSetLevel;
    }

    public String getJobLocation() {
        return jobLocation;
    }

    public void setJobLocation(String jobLocation) {
        this.jobLocation = jobLocation;
    }

    public long getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(long postedDate) {
        this.postedDate = postedDate;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getJobDesc() {
        return jobDesc;
    }

    public void setJobDesc(String jobDesc) {
        this.jobDesc = jobDesc;
    }

    public String getUserDetailsId() {
        return userDetailsId;
    }

    public void setUserDetailsId(String userDetailsId) {
        this.userDetailsId = userDetailsId;
    }

    @Override
    public String toString() {
        return "JobPostedDetailsBo{" +
                "jobLocation='" + jobLocation + '\'' +
                ", postedDate=" + postedDate +
                ", isActive='" + isActive + '\'' +
                ", jobType='" + jobType + '\'' +
                ", companyName='" + companyName + '\'' +
                ", jobDesc='" + jobDesc + '\'' +
                ", userDetailsId='" + userDetailsId + '\'' +
                ", skillSet='" + skillSet + '\'' +
                ", skillSetLevel='" + skillSetLevel + '\'' +
                '}';
    }
}
