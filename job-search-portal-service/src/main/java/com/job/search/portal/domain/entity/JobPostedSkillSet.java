package com.job.search.portal.domain.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="JOB_POSTED_SKILL_SET"
    ,schema="JOB_SEARCH_PORTAL"
)

public class JobPostedSkillSet  implements java.io.Serializable {
    // Fields

     private JobPostedSkillSetId id;
     private JobPostedDetails jobPostedDetails;
     private SkillSet skillSet;
     private String skillSetLevel;

    // Constructors

    /** default constructor */
    public JobPostedSkillSet() {
    }

	/** minimal constructor */
    public JobPostedSkillSet(JobPostedSkillSetId id, JobPostedDetails jobPostedDetails, SkillSet skillSet) {
        this.id = id;
        this.jobPostedDetails = jobPostedDetails;
        this.skillSet = skillSet;
    }
    
    /** full constructor */
    public JobPostedSkillSet(JobPostedSkillSetId id, JobPostedDetails jobPostedDetails, SkillSet skillSet, String skillSetLevel) {
        this.id = id;
        this.jobPostedDetails = jobPostedDetails;
        this.skillSet = skillSet;
        this.skillSetLevel = skillSetLevel;
    }

   
    // Property accessors
    @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="skillSetId", column=@Column(name="SKILL_SET_ID", nullable=false, length=200) ), 
        @AttributeOverride(name="jobPosterDetailsId", column=@Column(name="JOB_POSTER_DETAILS_ID", nullable=false, length=20) ) } )

    public JobPostedSkillSetId getId() {
        return this.id;
    }
    
    public void setId(JobPostedSkillSetId id) {
        this.id = id;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="JOB_POSTER_DETAILS_ID", nullable=false, insertable=false, updatable=false)

    public JobPostedDetails getJobPostedDetails() {
        return this.jobPostedDetails;
    }
    
    public void setJobPostedDetails(JobPostedDetails jobPostedDetails) {
        this.jobPostedDetails = jobPostedDetails;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="SKILL_SET_ID", nullable=false, insertable=false, updatable=false)

    public SkillSet getSkillSet() {
        return this.skillSet;
    }
    
    public void setSkillSet(SkillSet skillSet) {
        this.skillSet = skillSet;
    }
    
    @Column(name="SKILL_SET_LEVEL", length=20)

    public String getSkillSetLevel() {
        return this.skillSetLevel;
    }
    
    public void setSkillSetLevel(String skillSetLevel) {
        this.skillSetLevel = skillSetLevel;
    }
   








}