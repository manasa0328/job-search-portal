package com.job.search.portal.domain.entity;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * JobSeekerExperienceDetailsId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class JobSeekerExperienceDetailsId  implements java.io.Serializable {


    // Fields    

     private String jobName;
     private Timestamp endDate;
     private Timestamp startDate;
     private String userDetailsId;


    // Constructors

    /** default constructor */
    public JobSeekerExperienceDetailsId() {
    }

    
    /** full constructor */
    public JobSeekerExperienceDetailsId(String jobName, Timestamp endDate, Timestamp startDate, String userDetailsId) {
        this.jobName = jobName;
        this.endDate = endDate;
        this.startDate = startDate;
        this.userDetailsId = userDetailsId;
    }

   
    // Property accessors

    @Column(name="JOB_NAME", nullable=false, length=20)

    public String getJobName() {
        return this.jobName;
    }
    
    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    @Column(name="END_DATE", nullable=false, length=7)

    public Timestamp getEndDate() {
        return this.endDate;
    }
    
    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    @Column(name="START_DATE", nullable=false, length=7)

    public Timestamp getStartDate() {
        return this.startDate;
    }
    
    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    @Column(name="USER_DETAILS_ID", nullable=false, length=20)

    public String getUserDetailsId() {
        return this.userDetailsId;
    }
    
    public void setUserDetailsId(String userDetailsId) {
        this.userDetailsId = userDetailsId;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof JobSeekerExperienceDetailsId) ) return false;
		 JobSeekerExperienceDetailsId castOther = ( JobSeekerExperienceDetailsId ) other; 
         
		 return ( (this.getJobName()==castOther.getJobName()) || ( this.getJobName()!=null && castOther.getJobName()!=null && this.getJobName().equals(castOther.getJobName()) ) )
 && ( (this.getEndDate()==castOther.getEndDate()) || ( this.getEndDate()!=null && castOther.getEndDate()!=null && this.getEndDate().equals(castOther.getEndDate()) ) )
 && ( (this.getStartDate()==castOther.getStartDate()) || ( this.getStartDate()!=null && castOther.getStartDate()!=null && this.getStartDate().equals(castOther.getStartDate()) ) )
 && ( (this.getUserDetailsId()==castOther.getUserDetailsId()) || ( this.getUserDetailsId()!=null && castOther.getUserDetailsId()!=null && this.getUserDetailsId().equals(castOther.getUserDetailsId()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getJobName() == null ? 0 : this.getJobName().hashCode() );
         result = 37 * result + ( getEndDate() == null ? 0 : this.getEndDate().hashCode() );
         result = 37 * result + ( getStartDate() == null ? 0 : this.getStartDate().hashCode() );
         result = 37 * result + ( getUserDetailsId() == null ? 0 : this.getUserDetailsId().hashCode() );
         return result;
   }   





}