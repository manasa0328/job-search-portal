package com.job.search.portal.domain.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * JobPostedSkillSetId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class JobPostedSkillSetId  implements java.io.Serializable {


    // Fields    

     private String skillSetId;
     private String jobPosterDetailsId;


    // Constructors

    /** default constructor */
    public JobPostedSkillSetId() {
    }

    
    /** full constructor */
    public JobPostedSkillSetId(String skillSetId, String jobPosterDetailsId) {
        this.skillSetId = skillSetId;
        this.jobPosterDetailsId = jobPosterDetailsId;
    }

   
    // Property accessors

    @Column(name="SKILL_SET_ID", nullable=false, length=200)

    public String getSkillSetId() {
        return this.skillSetId;
    }
    
    public void setSkillSetId(String skillSetId) {
        this.skillSetId = skillSetId;
    }

    @Column(name="JOB_POSTER_DETAILS_ID", nullable=false, length=20)

    public String getJobPosterDetailsId() {
        return this.jobPosterDetailsId;
    }
    
    public void setJobPosterDetailsId(String jobPosterDetailsId) {
        this.jobPosterDetailsId = jobPosterDetailsId;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof JobPostedSkillSetId) ) return false;
		 JobPostedSkillSetId castOther = ( JobPostedSkillSetId ) other; 
         
		 return ( (this.getSkillSetId()==castOther.getSkillSetId()) || ( this.getSkillSetId()!=null && castOther.getSkillSetId()!=null && this.getSkillSetId().equals(castOther.getSkillSetId()) ) )
 && ( (this.getJobPosterDetailsId()==castOther.getJobPosterDetailsId()) || ( this.getJobPosterDetailsId()!=null && castOther.getJobPosterDetailsId()!=null && this.getJobPosterDetailsId().equals(castOther.getJobPosterDetailsId()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getSkillSetId() == null ? 0 : this.getSkillSetId().hashCode() );
         result = 37 * result + ( getJobPosterDetailsId() == null ? 0 : this.getJobPosterDetailsId().hashCode() );
         return result;
   }   





}