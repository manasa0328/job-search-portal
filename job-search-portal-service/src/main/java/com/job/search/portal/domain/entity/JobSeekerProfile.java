package com.job.search.portal.domain.entity;

import java.math.BigDecimal;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * JobSeekerProfile entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="JOB_SEEKER_PROFILE"
    ,schema="JOB_SEARCH_PORTAL"
)

public class JobSeekerProfile  implements java.io.Serializable {


    // Fields    

     private JobSeekerProfileId id;
     private UserDetails userDetails;
     private BigDecimal currentSalaryAnnually;


    // Constructors

    /** default constructor */
    public JobSeekerProfile() {
    }

	/** minimal constructor */
    public JobSeekerProfile(JobSeekerProfileId id, UserDetails userDetails) {
        this.id = id;
        this.userDetails = userDetails;
    }
    
    /** full constructor */
    public JobSeekerProfile(JobSeekerProfileId id, UserDetails userDetails, BigDecimal currentSalaryAnnually) {
        this.id = id;
        this.userDetails = userDetails;
        this.currentSalaryAnnually = currentSalaryAnnually;
    }

   
    // Property accessors
    @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="userName", column=@Column(name="USER_NAME", nullable=false, length=20) ), 
        @AttributeOverride(name="userDetailsId", column=@Column(name="USER_DETAILS_ID", nullable=false, length=20) ) } )

    public JobSeekerProfileId getId() {
        return this.id;
    }
    
    public void setId(JobSeekerProfileId id) {
        this.id = id;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumns( { 
        @JoinColumn(name="USER_DETAILS_ID", referencedColumnName="USER_DETAILS_ID", nullable=false, insertable=false, updatable=false), 
        @JoinColumn(name="USER_NAME", referencedColumnName="USER_TYPE_ID", nullable=false, insertable=false, updatable=false) } )

    public UserDetails getUserDetails() {
        return this.userDetails;
    }
    
    public void setUserDetails(UserDetails userDetails) {
        this.userDetails = userDetails;
    }
    
    @Column(name="CURRENT_SALARY_ANNUALLY", precision=22, scale=0)

    public BigDecimal getCurrentSalaryAnnually() {
        return this.currentSalaryAnnually;
    }
    
    public void setCurrentSalaryAnnually(BigDecimal currentSalaryAnnually) {
        this.currentSalaryAnnually = currentSalaryAnnually;
    }
   








}