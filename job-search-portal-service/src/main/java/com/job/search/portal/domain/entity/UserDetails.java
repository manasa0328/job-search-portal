package com.job.search.portal.domain.entity;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * UserDetails entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="USER_DETAILS"
    ,schema="JOB_SEARCH_PORTAL"
)

public class UserDetails  implements java.io.Serializable {


    // Fields    

     private UserDetailsId id;
     private UserType userType;
     private String emailId;
     private String password;
     private String gender;
     private String isActive;
     private Long phoneNum;
     private Set<JobSeekerEducationalDetails> jobSeekerEducationalDetailses = new HashSet<JobSeekerEducationalDetails>(0);
     private Set<JobSeekerSkillSet> jobSeekerSkillSets = new HashSet<JobSeekerSkillSet>(0);
     private Set<JobSeekerExperienceDetails> jobSeekerExperienceDetailses = new HashSet<JobSeekerExperienceDetails>(0);
     private Set<JobSeekerProfile> jobSeekerProfiles = new HashSet<JobSeekerProfile>(0);


    // Constructors

    /** default constructor */
    public UserDetails() {
    }

	/** minimal constructor */
    public UserDetails(UserDetailsId id, UserType userType, String emailId, String password) {
        this.id = id;
        this.userType = userType;
        this.emailId = emailId;
        this.password = password;
    }
    
    /** full constructor */
    public UserDetails(UserDetailsId id, UserType userType, String emailId, String password, String gender, String isActive, Long phoneNum, Set<JobSeekerEducationalDetails> jobSeekerEducationalDetailses, Set<JobSeekerSkillSet> jobSeekerSkillSets, Set<JobSeekerExperienceDetails> jobSeekerExperienceDetailses, Set<JobSeekerProfile> jobSeekerProfiles) {
        this.id = id;
        this.userType = userType;
        this.emailId = emailId;
        this.password = password;
        this.gender = gender;
        this.isActive = isActive;
        this.phoneNum = phoneNum;
        this.jobSeekerEducationalDetailses = jobSeekerEducationalDetailses;
        this.jobSeekerSkillSets = jobSeekerSkillSets;
        this.jobSeekerExperienceDetailses = jobSeekerExperienceDetailses;
        this.jobSeekerProfiles = jobSeekerProfiles;
    }

   
    // Property accessors
    @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="userDetailsId", column=@Column(name="USER_DETAILS_ID", nullable=false, length=20) ), 
        @AttributeOverride(name="userTypeId", column=@Column(name="USER_TYPE_ID", nullable=false, length=20) ) } )

    public UserDetailsId getId() {
        return this.id;
    }
    
    public void setId(UserDetailsId id) {
        this.id = id;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="USER_TYPE_ID", nullable=false, insertable=false, updatable=false)

    public UserType getUserType() {
        return this.userType;
    }
    
    public void setUserType(UserType userType) {
        this.userType = userType;
    }
    
    @Column(name="EMAIL_ID", nullable=false, length=20)

    public String getEmailId() {
        return this.emailId;
    }
    
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }
    
    @Column(name="PASSWORD", nullable=false, length=20)

    public String getPassword() {
        return this.password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    @Column(name="GENDER", length=1)

    public String getGender() {
        return this.gender;
    }
    
    public void setGender(String gender) {
        this.gender = gender;
    }
    
    @Column(name="IS_ACTIVE", length=1)

    public String getIsActive() {
        return this.isActive;
    }
    
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }
    
    @Column(name="PHONE_NUM", precision=10, scale=0)

    public Long getPhoneNum() {
        return this.phoneNum;
    }
    
    public void setPhoneNum(Long phoneNum) {
        this.phoneNum = phoneNum;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="userDetails")

    public Set<JobSeekerEducationalDetails> getJobSeekerEducationalDetailses() {
        return this.jobSeekerEducationalDetailses;
    }
    
    public void setJobSeekerEducationalDetailses(Set<JobSeekerEducationalDetails> jobSeekerEducationalDetailses) {
        this.jobSeekerEducationalDetailses = jobSeekerEducationalDetailses;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="userDetails")

    public Set<JobSeekerSkillSet> getJobSeekerSkillSets() {
        return this.jobSeekerSkillSets;
    }
    
    public void setJobSeekerSkillSets(Set<JobSeekerSkillSet> jobSeekerSkillSets) {
        this.jobSeekerSkillSets = jobSeekerSkillSets;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="userDetails")

    public Set<JobSeekerExperienceDetails> getJobSeekerExperienceDetailses() {
        return this.jobSeekerExperienceDetailses;
    }
    
    public void setJobSeekerExperienceDetailses(Set<JobSeekerExperienceDetails> jobSeekerExperienceDetailses) {
        this.jobSeekerExperienceDetailses = jobSeekerExperienceDetailses;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="userDetails")

    public Set<JobSeekerProfile> getJobSeekerProfiles() {
        return this.jobSeekerProfiles;
    }
    
    public void setJobSeekerProfiles(Set<JobSeekerProfile> jobSeekerProfiles) {
        this.jobSeekerProfiles = jobSeekerProfiles;
    }
   








}