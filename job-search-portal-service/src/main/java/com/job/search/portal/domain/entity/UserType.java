package com.job.search.portal.domain.entity;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * UserType entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="USER_TYPE"
    ,schema="JOB_SEARCH_PORTAL"
)

public class UserType  implements java.io.Serializable {


    // Fields    

     private String userTypeId;
     private String userTypeDesc;
     private Set<UserDetails> userDetailses = new HashSet<UserDetails>(0);


    // Constructors

    /** default constructor */
    public UserType() {
    }

	/** minimal constructor */
    public UserType(String userTypeId, String userTypeDesc) {
        this.userTypeId = userTypeId;
        this.userTypeDesc = userTypeDesc;
    }
    
    /** full constructor */
    public UserType(String userTypeId, String userTypeDesc, Set<UserDetails> userDetailses) {
        this.userTypeId = userTypeId;
        this.userTypeDesc = userTypeDesc;
        this.userDetailses = userDetailses;
    }

   
    // Property accessors
    @Id 
    
    @Column(name="USER_TYPE_ID", unique=true, nullable=false, length=20)

    public String getUserTypeId() {
        return this.userTypeId;
    }
    
    public void setUserTypeId(String userTypeId) {
        this.userTypeId = userTypeId;
    }
    
    @Column(name="USER_TYPE_DESC", nullable=false, length=20)

    public String getUserTypeDesc() {
        return this.userTypeDesc;
    }
    
    public void setUserTypeDesc(String userTypeDesc) {
        this.userTypeDesc = userTypeDesc;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="userType")

    public Set<UserDetails> getUserDetailses() {
        return this.userDetailses;
    }
    
    public void setUserDetailses(Set<UserDetails> userDetailses) {
        this.userDetailses = userDetailses;
    }
   








}