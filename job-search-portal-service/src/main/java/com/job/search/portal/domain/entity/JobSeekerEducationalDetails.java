package com.job.search.portal.domain.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * JobSeekerEducationalDetails entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="JOB_SEEKER_EDUCATIONAL_DETAILS"
    ,schema="JOB_SEARCH_PORTAL"
)

public class JobSeekerEducationalDetails  implements java.io.Serializable {


    // Fields    

     private String userDetailsId;
     private UserDetails userDetails;
     private String major;
     private String universityName;
     private String percentage;


    // Constructors

    /** default constructor */
    public JobSeekerEducationalDetails() {
    }

    
    /** full constructor */
    public JobSeekerEducationalDetails(String userDetailsId, UserDetails userDetails, String major, String universityName, String percentage) {
        this.userDetailsId = userDetailsId;
        this.userDetails = userDetails;
        this.major = major;
        this.universityName = universityName;
        this.percentage = percentage;
    }

   
    // Property accessors
    @Id 
    
    @Column(name="USER_DETAILS_ID", unique=true, nullable=false, length=20)

    public String getUserDetailsId() {
        return this.userDetailsId;
    }
    
    public void setUserDetailsId(String userDetailsId) {
        this.userDetailsId = userDetailsId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumns( { 
        @JoinColumn(name="USER_DETAILS_ID", referencedColumnName="USER_DETAILS_ID", unique=true, nullable=false, insertable=false, updatable=false), 
        @JoinColumn(name="DEGREE_NAME", referencedColumnName="USER_TYPE_ID", nullable=false, insertable=false, updatable=false) } )

    public UserDetails getUserDetails() {
        return this.userDetails;
    }
    
    public void setUserDetails(UserDetails userDetails) {
        this.userDetails = userDetails;
    }
    
    @Column(name="MAJOR", nullable=false, length=20)

    public String getMajor() {
        return this.major;
    }
    
    public void setMajor(String major) {
        this.major = major;
    }
    
    @Column(name="UNIVERSITY_NAME", nullable=false, length=20)

    public String getUniversityName() {
        return this.universityName;
    }
    
    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }
    
    @Column(name="PERCENTAGE", nullable=false, length=20)

    public String getPercentage() {
        return this.percentage;
    }
    
    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }
   








}