package com.job.search.portal.domain.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * JobSeekerSkillSet entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="JOB_SEEKER_SKILL_SET"
    ,schema="JOB_SEARCH_PORTAL"
)

public class JobSeekerSkillSet  implements java.io.Serializable {


    // Fields    

     private JobSeekerSkillSetId id;
     private UserDetails userDetails;
     private SkillSet skillSet;
     private String skillSetLevel;


    // Constructors

    /** default constructor */
    public JobSeekerSkillSet() {
    }

	/** minimal constructor */
    public JobSeekerSkillSet(JobSeekerSkillSetId id, UserDetails userDetails, SkillSet skillSet) {
        this.id = id;
        this.userDetails = userDetails;
        this.skillSet = skillSet;
    }
    
    /** full constructor */
    public JobSeekerSkillSet(JobSeekerSkillSetId id, UserDetails userDetails, SkillSet skillSet, String skillSetLevel) {
        this.id = id;
        this.userDetails = userDetails;
        this.skillSet = skillSet;
        this.skillSetLevel = skillSetLevel;
    }

   
    // Property accessors
    @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="userDetailsId", column=@Column(name="USER_DETAILS_ID", nullable=false, length=20) ), 
        @AttributeOverride(name="skillSetId", column=@Column(name="SKILL_SET_ID", nullable=false, length=20) ) } )

    public JobSeekerSkillSetId getId() {
        return this.id;
    }
    
    public void setId(JobSeekerSkillSetId id) {
        this.id = id;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumns( { 
        @JoinColumn(name="USER_DETAILS_ID", referencedColumnName="USER_DETAILS_ID", nullable=false, insertable=false, updatable=false), 
        @JoinColumn(name="SKILL_SET_ID", referencedColumnName="USER_TYPE_ID", nullable=false, insertable=false, updatable=false) } )

    public UserDetails getUserDetails() {
        return this.userDetails;
    }
    
    public void setUserDetails(UserDetails userDetails) {
        this.userDetails = userDetails;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="SKILL_SET_ID", nullable=false, insertable=false, updatable=false)

    public SkillSet getSkillSet() {
        return this.skillSet;
    }
    
    public void setSkillSet(SkillSet skillSet) {
        this.skillSet = skillSet;
    }
    
    @Column(name="SKILL_SET_LEVEL", length=20)

    public String getSkillSetLevel() {
        return this.skillSetLevel;
    }
    
    public void setSkillSetLevel(String skillSetLevel) {
        this.skillSetLevel = skillSetLevel;
    }
   








}