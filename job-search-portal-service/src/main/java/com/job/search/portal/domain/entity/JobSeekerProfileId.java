package com.job.search.portal.domain.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * JobSeekerProfileId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class JobSeekerProfileId  implements java.io.Serializable {


    // Fields    

     private String userName;
     private String userDetailsId;


    // Constructors

    /** default constructor */
    public JobSeekerProfileId() {
    }

    
    /** full constructor */
    public JobSeekerProfileId(String userName, String userDetailsId) {
        this.userName = userName;
        this.userDetailsId = userDetailsId;
    }

   
    // Property accessors

    @Column(name="USER_NAME", nullable=false, length=20)

    public String getUserName() {
        return this.userName;
    }
    
    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Column(name="USER_DETAILS_ID", nullable=false, length=20)

    public String getUserDetailsId() {
        return this.userDetailsId;
    }
    
    public void setUserDetailsId(String userDetailsId) {
        this.userDetailsId = userDetailsId;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof JobSeekerProfileId) ) return false;
		 JobSeekerProfileId castOther = ( JobSeekerProfileId ) other; 
         
		 return ( (this.getUserName()==castOther.getUserName()) || ( this.getUserName()!=null && castOther.getUserName()!=null && this.getUserName().equals(castOther.getUserName()) ) )
 && ( (this.getUserDetailsId()==castOther.getUserDetailsId()) || ( this.getUserDetailsId()!=null && castOther.getUserDetailsId()!=null && this.getUserDetailsId().equals(castOther.getUserDetailsId()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getUserName() == null ? 0 : this.getUserName().hashCode() );
         result = 37 * result + ( getUserDetailsId() == null ? 0 : this.getUserDetailsId().hashCode() );
         return result;
   }   





}