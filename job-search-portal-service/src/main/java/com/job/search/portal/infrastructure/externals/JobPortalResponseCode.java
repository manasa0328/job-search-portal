package com.job.search.portal.infrastructure.externals;

public enum JobPortalResponseCode {
    SUCCESS("JP001", "Job Details are posted successfully into DB"),
    FAIL("JP002", "Error Occurred, Please try again later "),
    INVALID_JSON("JP004", "Json Validation Error"),
    EXCEPTION("JP005", "Exception occurred");

    private String responseDescription;
    private String responseCode;

    JobPortalResponseCode(String responseDescription, String responseCode) {
        this.responseDescription = responseDescription;
        this.responseCode = responseCode;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseDescription() {
        return responseDescription;
    }

    public void setResponseDescription(String responseDescription) {
        this.responseDescription = responseDescription;
    }

}
