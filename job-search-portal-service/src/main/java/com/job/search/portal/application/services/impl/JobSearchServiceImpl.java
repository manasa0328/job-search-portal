package com.job.search.portal.application.services.impl;

import com.job.search.portal.application.services.JobSearchService;
import com.job.search.portal.domain.entity.JobPostedDetails;
import com.job.search.portal.domain.model.JobPostedDetailsBo;
import com.job.search.portal.infrastructure.repositories.CustomSequenceRepo;
import com.job.search.portal.infrastructure.repositories.JobPostDetailsRepo;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class JobSearchServiceImpl implements JobSearchService {
    private static final Log LOGGER = LogFactory.getLog(JobSearchServiceImpl.class);

    @Autowired JobPostDetailsRepo jobPostDetailsRepo;
    @Autowired CustomSequenceRepo customSequenceRepo;

    @Transactional
    @Override
    public boolean processJobPostDetails(JobPostedDetailsBo postedDetailsBo) throws Exception {
        try {
            JobPostedDetails jobPostedDetails = constructJobPostDetails(postedDetailsBo);
            jobPostDetailsRepo.save(jobPostedDetails);
            LOGGER.debug("Job is posted successfully ");
            return true;
        } catch (Exception exception) {
            LOGGER.error("Exception Occurred while job posting..... " + exception);
            throw new Exception(exception);
        }
    }

    @Override
    @Transactional
    public List<JobPostedDetails> searchJobPostedDetailsByKeyword(String skillSet, String companyName,
                                                                  String isActive, String jobLocation, String jobType) throws Exception {
        try {
            List<JobPostedDetails> jobPostedDetails = jobPostDetailsRepo.
                    findBySkillSetOrCompanyNameOrIsActiveOrJobLocationOrJobType(skillSet, companyName, isActive,
                            jobLocation, jobType);
            return jobPostedDetails;
        } catch (Exception exception) {
            LOGGER.debug("Exception Occurred while searching job details..... " + exception);
            throw new Exception(exception);
        }
    }

    @Override
    public List<JobPostedDetailsBo> publishBulkJobDetails(MultipartFile multipartFile) throws IOException {
        File file = convertMultiPartToFile(multipartFile);

        try (Reader reader = new FileReader(file);) {
            @SuppressWarnings("unchecked")
            CsvToBean<JobPostedDetailsBo> csvToBean = new CsvToBeanBuilder<JobPostedDetailsBo>(reader).
                    withType(JobPostedDetailsBo.class)
                    .withIgnoreLeadingWhiteSpace(true).build();
            List<JobPostedDetailsBo> jobPostedDetailsBos = csvToBean.parse();
            return jobPostedDetailsBos;
        } catch (IOException exception) {
            LOGGER.error("Exception in publishing the bulk job details... Could not publish CSV file ");
            throw new IOException(exception);
        }
    }

    @Override
    public void uploadBulkJobDetailsIntoDB(List<Map<Object, Object>> objectMapList) throws Exception {
        List<JobPostedDetails> jobPostedDetailsList = new ArrayList<>();
        Map<Object, Object> objectMap;
        try {
            for (int i = 0; i < objectMapList.size(); i++) {
                JobPostedDetails jobPostedDetails = new JobPostedDetails();
                objectMap = objectMapList.get(i);
                jobPostedDetails.setJobPosterDetailsId("JDP" + customSequenceRepo.customSeqForJobPostDetailsId());
                jobPostedDetails.setJobLocation(((String) objectMap.get("jobLocation")).toUpperCase());
                jobPostedDetails.setIsActive(((String) objectMap.get("isActive")).toUpperCase());
                jobPostedDetails.setJobType(((String) objectMap.get("jobType")).toUpperCase());
                jobPostedDetails.setCompanyName(((String) objectMap.get("companyName")).toUpperCase());
                jobPostedDetails.setPostedDate(new Timestamp(System.currentTimeMillis()));
                jobPostedDetails.setJobDesc(((String) objectMap.get("jobDesc")).toUpperCase());
                jobPostedDetails.setUserDetailsId(((String) objectMap.get("userDetailsId")).toUpperCase());
                jobPostedDetails.setSkillSet(((String) objectMap.get("skillSet")).toUpperCase());
                jobPostedDetails.setSkillSetLevel(((String) objectMap.get("skillSetLevel")).toUpperCase());
                jobPostedDetailsList.add(jobPostedDetails);
                objectMap.clear();
            }
            LOGGER.info("Size of the job details list " + jobPostedDetailsList.size());
            jobPostDetailsRepo.saveAll(jobPostedDetailsList);
        } catch (Exception exception) {
            LOGGER.error("Exception Occurred while inserting bulk data into JOB POSTED DETAILS table.. " + exception);
        }
    }

    private File convertMultiPartToFile(MultipartFile file) throws IOException {
        File convFile = new File(file.getOriginalFilename());
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }

    private JobPostedDetails constructJobPostDetails(JobPostedDetailsBo postedDetailsBo) throws Exception {
        try {
            JobPostedDetails jobPostedDetails = new JobPostedDetails();
            jobPostedDetails.setJobPosterDetailsId("JDP" + customSequenceRepo.customSeqForJobPostDetailsId());
            jobPostedDetails.setJobLocation(postedDetailsBo.getJobLocation().toUpperCase());
            jobPostedDetails.setIsActive(postedDetailsBo.getIsActive().toUpperCase());
            jobPostedDetails.setCompanyName(postedDetailsBo.getCompanyName().toUpperCase());
            jobPostedDetails.setPostedDate(new Timestamp(postedDetailsBo.getPostedDate()));
            jobPostedDetails.setJobDesc(postedDetailsBo.getJobDesc().toUpperCase());
            jobPostedDetails.setJobType(postedDetailsBo.getJobType().toUpperCase());
            jobPostedDetails.setUserDetailsId(postedDetailsBo.getUserDetailsId().toUpperCase());
            jobPostedDetails.setSkillSet(postedDetailsBo.getSkillSet().toUpperCase());
            jobPostedDetails.setSkillSetLevel(postedDetailsBo.getSkillSetLevel().toUpperCase());
            return jobPostedDetails;
        } catch (Exception exception) {
            throw new Exception("Exception while constructing Job post details from BO obj.. " + exception);
        }
    }
}
