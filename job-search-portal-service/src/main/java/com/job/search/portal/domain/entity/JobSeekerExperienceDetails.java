package com.job.search.portal.domain.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * JobSeekerExperienceDetails entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="JOB_SEEKER_EXPERIENCE_DETAILS"
    ,schema="JOB_SEARCH_PORTAL"
)

public class JobSeekerExperienceDetails  implements java.io.Serializable {


    // Fields    

     private JobSeekerExperienceDetailsId id;
     private UserDetails userDetails;
     private String isCurrentJob;
     private String companyName;
     private String jobLocationAddress;


    // Constructors

    /** default constructor */
    public JobSeekerExperienceDetails() {
    }

	/** minimal constructor */
    public JobSeekerExperienceDetails(JobSeekerExperienceDetailsId id, UserDetails userDetails, String isCurrentJob) {
        this.id = id;
        this.userDetails = userDetails;
        this.isCurrentJob = isCurrentJob;
    }
    
    /** full constructor */
    public JobSeekerExperienceDetails(JobSeekerExperienceDetailsId id, UserDetails userDetails, String isCurrentJob, String companyName, String jobLocationAddress) {
        this.id = id;
        this.userDetails = userDetails;
        this.isCurrentJob = isCurrentJob;
        this.companyName = companyName;
        this.jobLocationAddress = jobLocationAddress;
    }

   
    // Property accessors
    @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="jobName", column=@Column(name="JOB_NAME", nullable=false, length=20) ), 
        @AttributeOverride(name="endDate", column=@Column(name="END_DATE", nullable=false, length=7) ), 
        @AttributeOverride(name="startDate", column=@Column(name="START_DATE", nullable=false, length=7) ), 
        @AttributeOverride(name="userDetailsId", column=@Column(name="USER_DETAILS_ID", nullable=false, length=20) ) } )

    public JobSeekerExperienceDetailsId getId() {
        return this.id;
    }
    
    public void setId(JobSeekerExperienceDetailsId id) {
        this.id = id;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumns( { 
        @JoinColumn(name="USER_DETAILS_ID", referencedColumnName="USER_DETAILS_ID", nullable=false, insertable=false, updatable=false), 
        @JoinColumn(name="JOB_NAME", referencedColumnName="USER_TYPE_ID", nullable=false, insertable=false, updatable=false) } )

    public UserDetails getUserDetails() {
        return this.userDetails;
    }
    
    public void setUserDetails(UserDetails userDetails) {
        this.userDetails = userDetails;
    }
    
    @Column(name="IS_CURRENT_JOB", nullable=false, length=1)

    public String getIsCurrentJob() {
        return this.isCurrentJob;
    }
    
    public void setIsCurrentJob(String isCurrentJob) {
        this.isCurrentJob = isCurrentJob;
    }
    
    @Column(name="COMPANY_NAME", length=100)

    public String getCompanyName() {
        return this.companyName;
    }
    
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
    
    @Column(name="JOB_LOCATION_ADDRESS", length=1000)

    public String getJobLocationAddress() {
        return this.jobLocationAddress;
    }
    
    public void setJobLocationAddress(String jobLocationAddress) {
        this.jobLocationAddress = jobLocationAddress;
    }
   








}