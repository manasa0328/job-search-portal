package com.job.search.portal.infrastructure.repositories;

import com.job.search.portal.domain.entity.JobPostedDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JobPostDetailsRepo extends JpaRepository<JobPostedDetails, String> {
    List<JobPostedDetails> findBySkillSetOrCompanyNameOrIsActiveOrJobLocationOrJobType(String skillSet,
                                             String companyName, String isActive, String jobLocation, String jobType);
}
