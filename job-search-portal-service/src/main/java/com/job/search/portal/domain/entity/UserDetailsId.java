package com.job.search.portal.domain.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * UserDetailsId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class UserDetailsId  implements java.io.Serializable {


    // Fields    

     private String userDetailsId;
     private String userTypeId;


    // Constructors

    /** default constructor */
    public UserDetailsId() {
    }

    
    /** full constructor */
    public UserDetailsId(String userDetailsId, String userTypeId) {
        this.userDetailsId = userDetailsId;
        this.userTypeId = userTypeId;
    }

   
    // Property accessors

    @Column(name="USER_DETAILS_ID", nullable=false, length=20)

    public String getUserDetailsId() {
        return this.userDetailsId;
    }
    
    public void setUserDetailsId(String userDetailsId) {
        this.userDetailsId = userDetailsId;
    }

    @Column(name="USER_TYPE_ID", nullable=false, length=20)

    public String getUserTypeId() {
        return this.userTypeId;
    }
    
    public void setUserTypeId(String userTypeId) {
        this.userTypeId = userTypeId;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof UserDetailsId) ) return false;
		 UserDetailsId castOther = ( UserDetailsId ) other; 
         
		 return ( (this.getUserDetailsId()==castOther.getUserDetailsId()) || ( this.getUserDetailsId()!=null && castOther.getUserDetailsId()!=null && this.getUserDetailsId().equals(castOther.getUserDetailsId()) ) )
 && ( (this.getUserTypeId()==castOther.getUserTypeId()) || ( this.getUserTypeId()!=null && castOther.getUserTypeId()!=null && this.getUserTypeId().equals(castOther.getUserTypeId()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getUserDetailsId() == null ? 0 : this.getUserDetailsId().hashCode() );
         result = 37 * result + ( getUserTypeId() == null ? 0 : this.getUserTypeId().hashCode() );
         return result;
   }   





}