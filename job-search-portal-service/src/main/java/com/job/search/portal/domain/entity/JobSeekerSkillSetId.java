package com.job.search.portal.domain.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * JobSeekerSkillSetId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class JobSeekerSkillSetId  implements java.io.Serializable {


    // Fields    

     private String userDetailsId;
     private String skillSetId;


    // Constructors

    /** default constructor */
    public JobSeekerSkillSetId() {
    }

    
    /** full constructor */
    public JobSeekerSkillSetId(String userDetailsId, String skillSetId) {
        this.userDetailsId = userDetailsId;
        this.skillSetId = skillSetId;
    }

   
    // Property accessors

    @Column(name="USER_DETAILS_ID", nullable=false, length=20)

    public String getUserDetailsId() {
        return this.userDetailsId;
    }
    
    public void setUserDetailsId(String userDetailsId) {
        this.userDetailsId = userDetailsId;
    }

    @Column(name="SKILL_SET_ID", nullable=false, length=20)

    public String getSkillSetId() {
        return this.skillSetId;
    }
    
    public void setSkillSetId(String skillSetId) {
        this.skillSetId = skillSetId;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof JobSeekerSkillSetId) ) return false;
		 JobSeekerSkillSetId castOther = ( JobSeekerSkillSetId ) other; 
         
		 return ( (this.getUserDetailsId()==castOther.getUserDetailsId()) || ( this.getUserDetailsId()!=null && castOther.getUserDetailsId()!=null && this.getUserDetailsId().equals(castOther.getUserDetailsId()) ) )
 && ( (this.getSkillSetId()==castOther.getSkillSetId()) || ( this.getSkillSetId()!=null && castOther.getSkillSetId()!=null && this.getSkillSetId().equals(castOther.getSkillSetId()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getUserDetailsId() == null ? 0 : this.getUserDetailsId().hashCode() );
         result = 37 * result + ( getSkillSetId() == null ? 0 : this.getSkillSetId().hashCode() );
         return result;
   }   





}