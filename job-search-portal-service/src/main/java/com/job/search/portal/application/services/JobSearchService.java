package com.job.search.portal.application.services;

import com.job.search.portal.domain.entity.JobPostedDetails;
import com.job.search.portal.domain.model.JobPostedDetailsBo;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface JobSearchService {
    boolean processJobPostDetails(JobPostedDetailsBo jobPostedDetailsBo) throws Exception;

    List<JobPostedDetails> searchJobPostedDetailsByKeyword(String skillSet, String companyName, String isActive,
                                                           String jobLocation, String jobType) throws Exception;

    List<JobPostedDetailsBo> publishBulkJobDetails(MultipartFile multipartFile) throws IOException;

    void uploadBulkJobDetailsIntoDB(List<Map<Object,Object>> objectList) throws Exception;
}
