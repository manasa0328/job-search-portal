package com.job.search.portal.infrastructure.repositories;

import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Service
public class CustomSequenceRepo {

    public static final String SEQUENCE_NAME = "JOB_SEARCH_PORTAL.JOB_POSTER_DETAILS_SEQ";
    @PersistenceContext
    private EntityManager entityManager;

    public String customSeqForJobPostDetailsId() {
        Query query = entityManager.createNativeQuery("SELECT TO_CHAR (" + SEQUENCE_NAME + "." +
                "NEXTVAL,'0000000000') from DUAL");
        return query.getSingleResult().toString().trim();
    }
}
