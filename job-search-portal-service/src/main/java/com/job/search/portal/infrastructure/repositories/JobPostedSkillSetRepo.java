package com.job.search.portal.infrastructure.repositories;

import com.job.search.portal.domain.entity.JobPostedSkillSet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JobPostedSkillSetRepo extends JpaRepository<JobPostedSkillSet, String> {
}
