package com.job.search.portal.domain.entity;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * JobType entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="JOB_TYPE"
    ,schema="JOB_SEARCH_PORTAL"
)

public class JobType  implements java.io.Serializable {


    // Fields    

     private String jobTypeId;
     private String jobTypeDesc;

    // Constructors

    /** default constructor */
    public JobType() {
    }

	/** minimal constructor */
    public JobType(String jobTypeId) {
        this.jobTypeId = jobTypeId;
    }
    
    /** full constructor */
    public JobType(String jobTypeId, String jobTypeDesc, Set<JobPostedDetails> jobPostedDetailses) {
        this.jobTypeId = jobTypeId;
        this.jobTypeDesc = jobTypeDesc;
    }

   
    // Property accessors
    @Id 
    
    @Column(name="JOB_TYPE_ID", unique=true, nullable=false, length=20)

    public String getJobTypeId() {
        return this.jobTypeId;
    }
    
    public void setJobTypeId(String jobTypeId) {
        this.jobTypeId = jobTypeId;
    }
    
    @Column(name="JOB_TYPE_DESC", length=20)

    public String getJobTypeDesc() {
        return this.jobTypeDesc;
    }
    
    public void setJobTypeDesc(String jobTypeDesc) {
        this.jobTypeDesc = jobTypeDesc;
    }

}