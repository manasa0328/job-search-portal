package com.job.search.portal.infrastructure.repositories;

import com.job.search.portal.domain.entity.JobLocation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JobLocationRepo extends JpaRepository<JobLocation, String> {
}
