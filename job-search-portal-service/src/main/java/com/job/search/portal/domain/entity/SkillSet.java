package com.job.search.portal.domain.entity;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * SkillSet entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="SKILL_SET"
    ,schema="JOB_SEARCH_PORTAL"
)

public class SkillSet  implements java.io.Serializable {


    // Fields    

     private String skillSetId;
     private String skillSetName;
     private Set<JobPostedSkillSet> jobPostedSkillSets = new HashSet<JobPostedSkillSet>(0);
     private Set<JobSeekerSkillSet> jobSeekerSkillSets = new HashSet<JobSeekerSkillSet>(0);


    // Constructors

    /** default constructor */
    public SkillSet() {
    }

	/** minimal constructor */
    public SkillSet(String skillSetId) {
        this.skillSetId = skillSetId;
    }
    
    /** full constructor */
    public SkillSet(String skillSetId, String skillSetName, Set<JobPostedSkillSet> jobPostedSkillSets, Set<JobSeekerSkillSet> jobSeekerSkillSets) {
        this.skillSetId = skillSetId;
        this.skillSetName = skillSetName;
        this.jobPostedSkillSets = jobPostedSkillSets;
        this.jobSeekerSkillSets = jobSeekerSkillSets;
    }

   
    // Property accessors
    @Id 
    
    @Column(name="SKILL_SET_ID", unique=true, nullable=false, length=200)

    public String getSkillSetId() {
        return this.skillSetId;
    }
    
    public void setSkillSetId(String skillSetId) {
        this.skillSetId = skillSetId;
    }
    
    @Column(name="SKILL_SET_NAME", length=200)

    public String getSkillSetName() {
        return this.skillSetName;
    }
    
    public void setSkillSetName(String skillSetName) {
        this.skillSetName = skillSetName;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="skillSet")

    public Set<JobPostedSkillSet> getJobPostedSkillSets() {
        return this.jobPostedSkillSets;
    }
    
    public void setJobPostedSkillSets(Set<JobPostedSkillSet> jobPostedSkillSets) {
        this.jobPostedSkillSets = jobPostedSkillSets;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="skillSet")

    public Set<JobSeekerSkillSet> getJobSeekerSkillSets() {
        return this.jobSeekerSkillSets;
    }
    
    public void setJobSeekerSkillSets(Set<JobSeekerSkillSet> jobSeekerSkillSets) {
        this.jobSeekerSkillSets = jobSeekerSkillSets;
    }
   








}