package com.job.search.portal.domain.entity;

import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="JOB_LOCATION"
    ,schema="JOB_SEARCH_PORTAL"
)

public class JobLocation  implements java.io.Serializable {


    // Fields    

     private String jobLocationId;
     private String streetAddress;
     private String state;
     private String country;
     private String pincode;
     private String city;

    // Constructors

    /** default constructor */
    public JobLocation() {
    }

	/** minimal constructor */
    public JobLocation(String jobLocationId) {
        this.jobLocationId = jobLocationId;
    }
    
    /** full constructor */
    public JobLocation(String jobLocationId, String streetAddress, String state, String country, String pincode, String city, Set<JobPostedDetails> jobPostedDetailses) {
        this.jobLocationId = jobLocationId;
        this.streetAddress = streetAddress;
        this.state = state;
        this.country = country;
        this.pincode = pincode;
        this.city = city;
    }

   
    // Property accessors
    @Id 
    
    @Column(name="JOB_LOCATION_ID", unique=true, nullable=false, length=20)

    public String getJobLocationId() {
        return this.jobLocationId;
    }
    
    public void setJobLocationId(String jobLocationId) {
        this.jobLocationId = jobLocationId;
    }
    
    @Column(name="STREET_ADDRESS", length=20)

    public String getStreetAddress() {
        return this.streetAddress;
    }
    
    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }
    
    @Column(name="STATE", length=20)

    public String getState() {
        return this.state;
    }
    
    public void setState(String state) {
        this.state = state;
    }
    
    @Column(name="COUNTRY", length=20)

    public String getCountry() {
        return this.country;
    }
    
    public void setCountry(String country) {
        this.country = country;
    }
    
    @Column(name="PINCODE", length=20)

    public String getPincode() {
        return this.pincode;
    }
    
    public void setPincode(String pincode) {
        this.pincode = pincode;
    }
    
    @Column(name="CITY", length=20)

    public String getCity() {
        return this.city;
    }
    
    public void setCity(String city) {
        this.city = city;
    }

}