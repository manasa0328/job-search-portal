package com.job.search.portal.infrastructure.repositories;

import com.job.search.portal.domain.entity.JobLocation;
import com.job.search.portal.domain.entity.JobType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JobTypeRepo extends JpaRepository<JobType, String> {
}
