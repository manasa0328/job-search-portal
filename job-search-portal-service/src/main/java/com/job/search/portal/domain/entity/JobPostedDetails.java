package com.job.search.portal.domain.entity;

import java.sql.Timestamp;
import javax.persistence.*;

@Entity
@Table(name = "JOB_POSTED_DETAILS", schema = "JOB_SEARCH_PORTAL")

public class JobPostedDetails implements java.io.Serializable {

    private String jobPosterDetailsId;
    private String jobLocation;
    private String isActive;
    private String jobType;
    private String companyName;
    private Timestamp postedDate;
    private String jobDesc;
    private String userDetailsId;
    private String skillSet;
    private String skillSetLevel;

    // Constructors

    /** default constructor */
    public JobPostedDetails() {
    }

    /** minimal constructor */
    public JobPostedDetails(String jobPosterDetailsId, String userDetailsId) {
        this.jobPosterDetailsId = jobPosterDetailsId;
        this.userDetailsId = userDetailsId;
    }

    public JobPostedDetails(String jobPosterDetailsId, String jobLocation, String isActive, String jobType,
                            String companyName, Timestamp postedDate, String jobDesc, String userDetailsId, String skillSet,
                            String skillSetLevel) {
        this.jobPosterDetailsId = jobPosterDetailsId;
        this.jobLocation = jobLocation;
        this.isActive = isActive;
        this.jobType = jobType;
        this.companyName = companyName;
        this.postedDate = postedDate;
        this.jobDesc = jobDesc;
        this.userDetailsId = userDetailsId;
        this.skillSet = skillSet;
        this.skillSetLevel = skillSetLevel;
    }
    // Property accessors
    @Id

    @Column(name = "JOB_POSTER_DETAILS_ID", unique = true, nullable = false, length = 20)
    public String getJobPosterDetailsId() {
        return this.jobPosterDetailsId;
    }

    public void setJobPosterDetailsId(String jobPosterDetailsId) {
        this.jobPosterDetailsId = jobPosterDetailsId;
    }

    @Column(name = "JOB_LOCATION", length = 20)

    public String getJobLocation() {
        return this.jobLocation;
    }

    public void setJobLocation(String jobLocation) {
        this.jobLocation = jobLocation;
    }

    @Column(name = "IS_ACTIVE", length = 20)

    public String getIsActive() {
        return this.isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    @Column(name = "JOB_TYPE", length = 20)

    public String getJobType() {
        return this.jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    @Column(name = "COMPANY_NAME", length = 20)

    public String getCompanyName() {
        return this.companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    @Column(name = "POSTED_DATE", length = 7)

    public Timestamp getPostedDate() {
        return this.postedDate;
    }

    public void setPostedDate(Timestamp postedDate) {
        this.postedDate = postedDate;
    }

    @Column(name = "JOB_DESC", length = 20)

    public String getJobDesc() {
        return this.jobDesc;
    }

    public void setJobDesc(String jobDesc) {
        this.jobDesc = jobDesc;
    }

    @Column(name = "USER_DETAILS_ID", nullable = false, length = 20)

    public String getUserDetailsId() {
        return this.userDetailsId;
    }

    public void setUserDetailsId(String userDetailsId) {
        this.userDetailsId = userDetailsId;
    }

    @Column(name = "SKILL_SET", length = 1000)

    public String getSkillSet() {
        return this.skillSet;
    }

    public void setSkillSet(String skillSet) {
        this.skillSet = skillSet;
    }

    @Column(name = "SKILL_SET_LEVEL", length = 20)

    public String getSkillSetLevel() {
        return this.skillSetLevel;
    }

    public void setSkillSetLevel(String skillSetLevel) {
        this.skillSetLevel = skillSetLevel;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "jobPostedDetails")

    @Override
    public String toString() {
        return "JobPostedDetails{" +
                "jobPosterDetailsId='" + jobPosterDetailsId + '\'' +
                ", jobLocation='" + jobLocation + '\'' +
                ", isActive='" + isActive + '\'' +
                ", jobType='" + jobType + '\'' +
                ", companyName='" + companyName + '\'' +
                ", postedDate=" + postedDate +
                ", jobDesc='" + jobDesc + '\'' +
                ", userDetailsId='" + userDetailsId + '\'' +
                ", skillSet='" + skillSet + '\'' +
                ", skillSetLevel='" + skillSetLevel + '\'' +
                '}';
    }
}