# job-search-portal

Prerequisites:

1. Windows operating system
2. Java 1.8
3. Maven must be installed in the system to build and run the application

Getting Started: 

1. Clone the git repo in to your local directory (in F:/drive) from the below git repository https://gitlab.com/manasa0328/job-search-portal.git 
2. Open the command prompt and navigate to the main project directory, F:\job-search-portal
3. Execute the command ‘mvn clean install’
4. Navigate to webapp directory and start the spring boot server by executing the command  ‘mvn spring-boot:run’  F:\job-search-portal\job-search-portal-webapp> mvn spring-boot:run

Endpoints Exposed: 

1. Insert API which accepts post parameters for single job, 
URL: http://localhost:9000/job-search/job-post-details
Method type: POST
Request Body:   
{
"jobLocation": "Chennai",
"postedDate":"24-OCT-18",
"isActive":"Y",
"jobType": "Full time",
"companyName":"Risk sense",
"jobDesc":"JAVA Developer",
"userDetailsId":"RECRUTIER",
"skills":{
"skillSet":"Java",
"skillLevel":"Beginner"
}
}


 


2.Bulk Insert API which publishes the job details to Kafka Producer in the csv format and consumes the file from Kafka Consumer and inserts into Oracle DB
URL: http://localhost:9000/job-search/bulk-upload-job-details 
Method Type: POST
Form-data: file : <upload the csv file> (I have attached the CSV file in the gitlab as well)

 
On posting the data, Kafka consumer consumes the data and inserts into DB, 
 

 


3.Search API based on some of the fields to retrieve the data from the database on job posted details, 

URL :   http://localhost:9000/job-search/job-post-details/searchBy?companyName=TCS&jobType=Part time&jobLocation=Banglore&skillSet=Spring&isActive=Y
Method Type: GET
 


Configuration in property file under /resources:
server.port=9000

#Datasource Details
spring.datasource.url= jdbc:oracle:thin:@//localhost:1521/xe
spring.datasource.username=system
spring.datasource.password=admin
spring.jpa.database-platform=org.hibernate.dialect.Oracle10gDialect

#Kafka Properties
kafka.bootstrap-servers=localhost:9092
kafka.consumer.group-id=kafka-group
kafka.topic.name=kafka-topic

Steps to Configure and Run Kafka:
1.	Download latest version of kafka and install it. 
2.	Go to your Kafka installation directory C:\Tools\kafka_2.11-2.0.0>
3.	To start the zookeeper execute the following command,  
              bin\windows\zookeeper-server-start.bat config\zookeeper.properties
4.	To start the Kafka server execute the following command,
                 bin\windows\kafka-server-start.bat config\server.properties




